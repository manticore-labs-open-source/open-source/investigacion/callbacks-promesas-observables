const estudiantesEPN = require('./variables');
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');

function darExamenFinCarreraObservable(estudianteADarExamen) {

    estudianteADarExamen = JSON.parse(JSON.stringify(estudiantesEPN.estudiantes));

    return Observable.of(estudianteADarExamen)
                .pipe(
                    map(({ nombre }) => nombre)
                )
}

const observable$ = darExamenFinCarreraObservable('Cristian Lara')

observable$.subscribe((respuesta) => {
    console.log(`El estudiante ${respuesta} debe acercarse a dar el examen de fin de carrera`)
})