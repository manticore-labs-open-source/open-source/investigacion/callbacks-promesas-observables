const estudiantesEPN = require('./variables');

function darExamenFinCarrera(estudianteADarExamen, callback) {

    estudianteADarExamen = JSON.parse(JSON.stringify(estudiantesEPN.estudiantes));

    console.log('El estudiante es: ', estudianteADarExamen.nombre);
    console.log('¿Tiene todos los créditos aprobados?', estudianteADarExamen.todosCreditosAprobados);

    if(estudianteADarExamen.todosCreditosAprobados) {
        callback();
    }

    else {
        console.log('No puede dar el exámen de fin de carrera');
    }
}

function examenFinCarrera() {
    console.log('Favor de presentarse al aula respectiva para dar el exámen');
}

darExamenFinCarrera('Cristian Lara', examenFinCarrera);