const estudiantesEPN = require('./variables');

function darExamenFinCarreraPromesa(estudianteADarExamen) {

    estudianteADarExamen = JSON.parse(JSON.stringify(estudiantesEPN.estudiantes));

    return new Promise((resolve, reject) => {
        if(estudianteADarExamen.todosCreditosAprobados) {
            resolve({
                Estudiante: estudianteADarExamen.nombre,
                TodosCreditosAprobados: estudianteADarExamen.todosCreditosAprobados, 
                Mensaje: "Preséntese a dar el exámen de fin de carrera"
            })
        }

        else {
            reject({
                Estudiante: estudianteADarExamen.nombre,
                TodosCreditosAprobados: estudianteADarExamen.TodosCreditosAprobados, 
                Mensaje: "No puede dar el examen de fin de carrera"
            })
        }
    })
}

darExamenFinCarreraPromesa('Cristian Lara').then((respuesta) => {
    console.log(respuesta);
}).catch((errorPromesa) => {
    console.log(errorPromesa);
});