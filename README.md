# Creación de callbacks, promesas y observables.

Antes de comenzar, cabe aclarar que debido a unos problemas con la librería rxjs, la salida de observables no se encuentra disponible, sin embargo, se describirá la sintaxis para su creación y también el código está disponible. Todos los scripts utilizados para las posteriores explicaciones se encuentra disponibles en la carpeta 

## Callbacks

### Definición

Un callback no es más que una función que se utiliza como parámentro dentro de otra función. 

### Creación

La creación de un callback es bastante sencilla, pues se trata nada más de una función, por tanto, es necesario crear dos funciones: una que será el callback y otra que será la función que la utilice como parámetro.

### Ejemplo

Para ejemplificar su funcionamiento y uso, se tiene lo siguiente:

```
function darExamenFinCarrera(estudianteADarExamen, callback) {

    estudianteADarExamen = JSON.parse(JSON.stringify(estudiantesEPN.estudiantes));

    console.log('El estudiante es: ', estudianteADarExamen.nombre);
    console.log('¿Tiene todos los créditos aprobados?', estudianteADarExamen.todosCreditosAprobados);

    if(estudianteADarExamen.todosCreditosAprobados) {
        callback();
    }

    else {
        console.log('No puede dar el exámen de fin de carrera');
    }
}

function examenFinCarrera() {
    console.log('Favor de presentarse al aula respectiva para dar el exámen');
}

darExamenFinCarrera('Cristian Lara', examenFinCarrera);
```

Como se puede observar, existen dos funciones: la función *_examenFinCarrera()_* viene a ser el callback, pues es utilizada en la función *_darExamenFinCarrera()_* como uno de sus parámetros.

### Salida

![Callback](./imagenes/callback.png)

## Promesas

### Definición

Una promesa consiste en un objeto que demuestra el fallo o el éxito de una operación asíncrona.

### Creación

La creación de promesas se puede realizar dentro de una función a través de la siguiente sintaxis: 

```
return new Promise((resolve, reject) => {
    resolve: ({
        //Aquí mensaje en caso de éxito
    })

    reject: ({
        //Aquí mensaje en caso de fallo
    })
})
```
### Uso

Para invocar una promesa, es necesario utilizar *_.then()_* seguido de un *_.catch()_* al momento de llamar a la promesa, así:

```
promesa.then((exito) => {
    console.log(exito); 
    }).catch((error) => {
        console.log(error);
    })
```

### Ejemplo

Para ejemplificar el uso de promesas, basándose en el ejemplo anterior de callbacks, se tiene:

```
const estudiantesEPN = require('./variables');

function darExamenFinCarreraPromesa(estudianteADarExamen) {

    estudianteADarExamen = JSON.parse(JSON.stringify(estudiantesEPN.estudiantes));

    return new Promise((resolve, reject) => {
        if(estudianteADarExamen.todosCreditosAprobados) {
            resolve({
                Estudiante: estudianteADarExamen.nombre,
                TodosCreditosAprobados: estudianteADarExamen.todosCreditosAprobados, 
                Mensaje: "Preséntese a dar el exámen de fin de carrera"
            })
        }

        else {
            reject({
                Estudiante: estudianteADarExamen.nombre,
                TodosCreditosAprobados: estudianteADarExamen.TodosCreditosAprobados, 
                Mensaje: "No puede dar el examen de fin de carrera"
            })
        }
    })
}

darExamenFinCarreraPromesa('Cristian Lara').then((respuesta) => {
    console.log(respuesta);
}).catch((errorPromesa) => {
    console.log(errorPromesa);
});
```

### Salida

![Promesa](./imagenes/promesa.png)

## Observables

### Definición

Los observables pueden ser considerados como una interfaz y son la base de la programación reactiva.

### Creación

Para la creación de observables, en primer lugar, es necesario instalar la librería **rxjs** a través del comando *_npm i rxjs_*. Ahora bien, si se quiere transformar un conjunto de elementos en un observable, se hace uso de la función *_.of()_*, otras funciones útiles son, por el ejemplo *_.pipe()_* para agregar funcionalidad a una cadena. Con esto, la estructura para un observable sería, por ejemplo: 

```
return Observable.of([{ 'nombre': 'Andrés' }])
        .pipe(map({ nombre } => nombre))
```

### Uso

Para utilizar el observable, es necesario hacer uso de *_.subscribe()_* así:

```
observable$.subscribe((respuesta) => {
    console.log(respuesta)
})
```

### Ejemplo

```
const estudiantesEPN = require('./variables');
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');

function darExamenFinCarreraObservable(estudianteADarExamen) {

    estudianteADarExamen = JSON.parse(JSON.stringify(estudiantesEPN.estudiantes));

    return Observable.of(estudianteADarExamen)
                .pipe(
                    map({ nombre } => nombre)
                )
}

const observable$ = darExamenFinCarreraObservable('Cristian Lara')

observable$.subscribe((respuesta) => {
    console.log(`El estudiante ${respuesta} debe acercarse a dar el examen de fin de carrera`)
})
```

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>

